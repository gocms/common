package util

import (
	"database/sql"
	"errors"
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"gorm.io/gorm/schema"
	"io/ioutil"
	"s_server/common/obj"
	"testing"
	"time"
)

var (
	Ser *gorm.DB
	Set = obj.DBSetting{
		UserName: "root",
		Password: "qwer.1324",
		DataBase: "demo",
		Url:      "127.0.0.1",
		Port:     "3306",
		LogMode:  true,
	}
)

func OpenTestConnection() (dbs *gorm.DB, err error) {
	dbs, err = gorm.Open(
		mysql.Open(fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
			Set.UserName, Set.Password, Set.Url, Set.Port, Set.DataBase)), &gorm.Config{})
	if err != nil {
		fmt.Println("数据库连接失败" + err.Error())
		return nil, err
	}
	dbs.NamingStrategy = schema.NamingStrategy{
		//TablePrefix:   "fv_", // 表名前缀，`User` 的表名应该是 `t_users`
		SingularTable: true, // 使用单数表名，启用该选项，此时，`User` 的表名应该是 `fv_user`
		//ENGINE
	}
	//dbs.Logger = nil
	if Set.LogMode {
		dbs.Logger = dbs.Logger.LogMode(logger.Info)
	} else {
		dbs.Logger = dbs.Logger.LogMode(logger.Silent)
	}

	// SetMaxIdleConns 设置空闲连接池中连接的最大数量
	sqlDB, err := dbs.DB()
	if err != nil {
		fmt.Println("数据库连接失败" + err.Error())
		return nil, err
	}

	sqlDB.SetMaxIdleConns(50)
	// SetMaxOpenConns 设置打开数据库连接的最大数量。
	sqlDB.SetMaxOpenConns(1000)

	// SetConnMaxLifetime 设置了连接可复用的最大时间。
	sqlDB.SetConnMaxLifetime(time.Hour)
	return
}
func SqlInit(q func()) (err error) {
SqlLink:
	if Ser, err = OpenTestConnection(); err != nil {
		err = ResetSql()
		if err != nil {
			return err
		}
		goto SqlLink
	} else {
		sqlDB, err := Ser.DB()
		if err == nil {
			err = sqlDB.Ping()
		}
		if err != nil {
			return err
			//goto SqlLink
		}
		q()
	}
	return
}
func ResetSql() (err error) {
	//util.LogInfo("数据库连接失败" + err.Error())
	dbSer, err := sql.Open("mysql", fmt.Sprintf("%s:%s@tcp(%s:%s)/",
		Set.UserName, Set.Password, Set.Url, Set.Port))
	//关闭数据库连接
	defer dbSer.Close()
	if err != nil {
		LogInfo(err.Error())
		return err
	} else {
		LogInfo("数据库连接成功")
	}
	_, err = dbSer.Exec("CREATE DATABASE " + Set.DataBase + " default character set = 'utf8mb4';")
	if err != nil {
		LogInfo(err.Error())
		return errors.New(Set.DataBase + "数据库创建失败" + err.Error())
	} else {
		LogInfo(Set.DataBase + "数据库连接成功")
	}
	return
}
func init() {
}
func TestSqlCreate(t *testing.T) {
	SqlInit(q)
}

type User struct {
	Id     int     `json:"id"`
	Name   string  `json:"'name'" gorm:"type:text"`
	Price  float64 `json:"price" gorm:"type:decimal(10,2)"`
	Status int     `json:"status" gorm:"default:1;comment:工伤保险 状态 0 未缴纳 1 已缴纳;"`
}

func q() {
	fmt.Println(Ser.Set("gorm:table_options", "ENGINE=InnoDB").
		AutoMigrate(
			User{},
		))
}

func TestSqlToObj(t *testing.T) {
	b, _ := ioutil.ReadFile("d:/sql.sql")
	str := string(b)
	fmt.Println(getAll("tinyint", str))
	// 寻找主键
	//获取表单对象命名
	//CREATE TABLE `tb_salary_history`

	// 转换驼峰命名法
	// 备注
	// 时间格式
}

func TestRegexpFun(t *testing.T) {

	str := `
CREATE TABLE “tb_company“ (
  “id“ varchar(33) NOT NULL,
  “company_name“ varchar(100) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '公司名称',
  “legal“ varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '法人代表',
  “company_type“ varchar(50) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '公司性质',
  “company_phone“ varchar(20) DEFAULT NULL COMMENT '联系人电话',
  “company_address“ varchar(500) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '公司地址',
  “company_phone2“ varchar(20) DEFAULT NULL COMMENT '联系人电话2',
  “contacts“ varchar(20) DEFAULT NULL COMMENT '联系人',
  “bank_id“ varchar(33) DEFAULT NULL COMMENT '银行id',
  “bank_card_num“ varchar(30) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '银行卡号',
  “business_license“ varchar(20) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '营业执照',
  “general_item“ varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '营业范围',
  “trades“ text CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '工种 多个以分号隔开',
  “industrial_injury_insurance“ tinyint(2) NOT NULL DEFAULT 1 COMMENT '工伤保险 状态 0 未缴纳 1 已缴纳',
  “stars“ tinyint(2) NOT NULL DEFAULT 0 COMMENT '企业星级 默认五颗星',
  “advance_payment_status“ tinyint(2) NOT NULL DEFAULT 0 COMMENT '保障金状态 0 无数据 1正常  2 警戒线 3 余额不足',
  “advance_payment_amount“ decimal(10,2) NOT NULL DEFAULT 0.00 COMMENT '保障金余额',
  “contract_type“ varchar(50) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '合同类型 多个以分号隔开',
  “picture1“ varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL COMMENT '营业执照',
  “picture2“ varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  “picture3“ varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  “picture4“ varchar(100) DEFAULT NULL,
  “picture5“ varchar(100) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  “update_key“ bigint(20) NOT NULL DEFAULT 0 COMMENT '企业工程预储金更新时间戳',
  “create_time“ datetime DEFAULT NULL COMMENT '创建日期',
  “create_user“ varchar(33) DEFAULT NULL COMMENT '创建人id',
  “update_time“ datetime DEFAULT NULL,
  “update_user“ varchar(33) DEFAULT NULL COMMENT '更新人id',
  PRIMARY KEY (“id“) USING BTREE,
  UNIQUE KEY “id“ (“id“) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC COMMENT='公司信息表';
`

	var s SqlObj
	s.Json = "aBb"
	s.In(str)
}
