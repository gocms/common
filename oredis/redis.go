package oredis

import (
	"errors"
	"fmt"
	"github.com/go-redis/redis"
	"github.com/vmihailenco/msgpack"
	"runtime"
	"time"
)

type Client struct {
	New *redis.Client
}

var (
	Ser *Client
)

func New(option redis.Options) *Client {
	Ser = &Client{New: redis.NewClient(&option)}
	return Ser
}
func (c *Client) SetType(key string, val interface{}, expiration time.Duration) (err error) {
	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	b, err := msgpack.Marshal(val)
	if err != nil {
		return
	}
	_, err = c.New.Set(key, b, expiration).Result()
	if err != nil {
		return
	}
	return
}

func (c *Client) LPushExpire(key string, expiration time.Duration, values ...interface{}) (err error) {
	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	_, err = c.New.LPush(key, values...).Result()
	if err != nil {
		return
	}
	c.New.Expire(key, expiration)
	return
}
func (c *Client) LPush(key string, values ...interface{}) (err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	_, err = c.New.LPush(key, values...).Result()
	if err != nil {
		return
	}
	return
}
func (c *Client) LLen(key string) (a int64, err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	return c.New.LLen(key).Result()
}
func (c *Client) LRange(key string, start, stop int64) (list []string, err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	return c.New.LRange(key, start, stop).Result()
}

func (c *Client) HSETExpire(key string, expiration time.Duration, values ...interface{}) (err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	_, err = c.New.LPush(key, values...).Result()
	if err != nil {
		return
	}
	c.New.Expire(key, expiration)
	return
}
func (c *Client) HSET(name, key string, value interface{}) (err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	_, err = c.New.HSet(name, key, value).Result()
	if err != nil {
		return
	}
	return
}
func (c *Client) HGET(name, key string) (st string, err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	return c.New.HGet(name, key).Result()
}
func (c *Client) HDel(name, key string) (i int64, err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	return c.New.HDel(name, key).Result()
}
func (c *Client) HSETType(name, key string, value interface{}) (err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	b, err := msgpack.Marshal(value)
	if err != nil {
		return
	}
	c.HSET(name, key, b)
	return
}
func (c *Client) HGETType(name, key string, value interface{}) (err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	b, err := c.HGET(name, key)
	if err != nil {
		return err
	}
	_ = msgpack.Unmarshal([]byte(b), &value)
	return nil
}
func (c *Client) HLEN(name string) (i int64, err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	return c.New.HLen(name).Result()
}
func (c *Client) HSCAN(name string, offset, limit int64) (val map[string]string, err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	val = make(map[string]string)
	iter := c.New.HScan(name, uint64(offset), "", limit).Iterator()
	var i = 0
	var k string
	for iter.Next() {
		if i == 0 {
			k = iter.Val()
			val[k] = ""
			i++
		} else {
			val[k] = iter.Val()
			i = 0
		}
	}
	return val, nil
}
func (c *Client) HSCANAll(name string) (val map[string]string, err error) {
	val = make(map[string]string)
	limit, err := c.HLEN(name)
	if err != nil {
		return
	}
	iter := c.New.HScan(name, 0, "", limit*2).Iterator()
	var i = 0
	var k string
	for iter.Next() {
		if i == 0 {
			k = iter.Val()
			val[k] = ""
			i++
		} else {
			val[k] = iter.Val()
			i = 0
		}
	}
	return val, nil
}

func (c *Client) Set(key string, val string, expiration time.Duration) (err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	_, err = c.New.Set(key, val, expiration).Result()
	if err != nil {
		return
	}
	return
}

func (c *Client) SetKVType(key string, val interface{}) (err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	return c.SetType(key, val, 0)
}
func (c *Client) SetKV(key, val string) (err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	return c.Set(key, val, 0)
}

func (c *Client) Get(key string) (str string, err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	return c.New.Get(key).Result()
}
func (c *Client) GetType(key string, v interface{}) (err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	b, err := c.New.Get(key).Bytes()
	if err != nil {
		return
	}
	_ = msgpack.Unmarshal(b, &v)
	return
}
func (c *Client) Del(keys ...string) (i int64, err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	return c.New.Del(keys...).Result()
}
func (c *Client) Keys(keys string) (list []string, err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	return c.New.Keys(keys).Result()
}

func (c *Client) SetNX(key string, val interface{}, expiration time.Duration) (i bool, err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	return c.New.SetNX(key, val, expiration).Result()
}
func (c *Client) TTL(keys string) (y time.Duration, err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	return c.New.TTL(keys).Result()
} // Redis 剩下有效时间 单位秒
func (c *Client) Close() error {
	return c.New.Close()
}

func (c *Client) GetTimeDataTypeByTime(key string, v interface{}, t int64) (err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	var data TimeData
	err = c.GetType(key, &data)
	if err != nil {
		return
	}
	if t < data.StartTime.Unix() {
		return errors.New(fmt.Sprintf("未达到开始时间,还剩余%d", data.StartTime.Unix()-t))
	}
	if t > data.EndTime.Unix() {
		return errors.New(fmt.Sprintf("已超时"))
	}
	_ = msgpack.Unmarshal(data.Data, &v)
	return
} // redis 保存有效时间
func (c *Client) GetTimeDataType(key string, v interface{}) (err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	return c.GetTimeDataTypeByTime(key, v, time.Now().Unix())
} // redis 保存有效时间

func (c *Client) SetTimeData(key string, val interface{}, start, endTime time.Time) (err error) {

	defer func() {
		var r any = recover()
		switch r.(type) {
		case runtime.Error:
			err = fmt.Errorf("%v", r)
			return
		}
	}()
	b, err := msgpack.Marshal(val)
	if err != nil {
		return
	}
	var td = TimeData{
		EndTime:   endTime,
		StartTime: start,
		Data:      b,
	}
	t := time.Now().Unix()
	t = endTime.Unix() - t + 60
	err = c.SetType(key, td, time.Duration(t)*time.Second)
	if err != nil {
		return
	}
	return
}

type TimeData struct {
	EndTime   time.Time //到期时间
	StartTime time.Time // 开始时间
	Data      []byte    // 数据
} // 时间对象存入redis，用来判断诗句超时现象
