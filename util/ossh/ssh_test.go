package ossh

import (
	"database/sql"
	"fmt"
	"github.com/go-sql-driver/mysql"
	"testing"
)

func TestSshInit(t *testing.T) {
	SSHInit()
}

func SSHInit() {
	client, err := OSSHClient.DialWithPassword("10.10.10.201:22", "root", "@YiCeBa&2019.")
	if err != nil {
		panic(err)
	}
	out, err := client.Cmd("ls -l").Output()
	if err != nil {
		panic(err)
	}
	fmt.Println(string(out))
	// Now we register the ViaSSHDialer with the ssh connection as a parameter
	mysql.RegisterDialContext("mysql+tcp", (&ViaSSHDialer{client.Client, nil}).Dial)
	//mysql.RegisterDial("mysql+tcp", (&ViaSSHDialer{client.client}).Dial)
	if db, err := sql.Open("mysql", fmt.Sprintf("%s:%s@mysql+tcp(%s)/%s", "itcast", "Yi_Ce*Ba@2019-", "127.0.0.1:8066", "itcast")); err == nil {
		fmt.Printf("Successfully connected to the db\n")
		if rows, err := db.Query("SELECT id,`name` FROM tb_user ORDER BY id limit 10"); err == nil {
			for rows.Next() {
				var id int64
				var name string
				rows.Scan(&id, &name)
				fmt.Printf("ID: %d  Name: %s\n", id, name)
			}
			rows.Close()
		} else {
			fmt.Printf("Failure: %s", err.Error())
		}

		db.Close()
	}
}
